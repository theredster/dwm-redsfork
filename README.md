# dwm-redsfork

Whatcha expect? This is the source code for my fork of the window manager "DWM". Configured in the C programming language.

Made for v6.4.

Theme: Nord

![dwm-prev](https://gitlab.com/theredster/dwm-redsfork/-/raw/main/dwm-prev.png)

## List of patches

NOTE: I don't fully recall on what patches were used for this. So this list isn't 100% accurate.

- actualfullscreen

- alpha

- autostart

- uselessgap

- sticky

- restartsig
